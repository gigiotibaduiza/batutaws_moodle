from django.apps import AppConfig


class BatutaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'apps.Batuta'
