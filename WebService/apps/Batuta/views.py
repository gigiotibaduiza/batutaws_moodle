from ast import Expression
from asyncio import as_completed
import logging
from pickle import NONE
import sqlite3
from platform import libc_ver
from random import random
from statistics import variance
from unicodedata import category, name
from unittest import result
from django.http import response
from django.shortcuts import render
from django import forms
from django.http.response import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.urls.base import clear_script_prefix
#from rest_framework import generics, viewsets
from django.views.generic import TemplateView
from django.urls import reverse_lazy
from django.views import generic
from WebService.settings import DEBUG
from .services import *
import mysql.connector
import requests
import json
import threading
from .models import *
import time
import sched
from datetime import datetime
from time import sleep
import logging
from WebService.settings import *
from concurrent.futures import Executor, ThreadPoolExecutor
global time_at_id, time_courses,  time_teachers, time_update_enrollments,time_new_enrollments, time_insert_group, time_update_teachers, time_update_eid, time_query
time_at_id = False
time_update_eid = False
time_courses = False
time_teachers = False
time_update_teachers = False
time_groups = False
time_update_enrollments = False
time_new_enrollments = False
time_insert_group = False
time_query =False
global variable_log
variable_log = []
#logging.basicConfig(filename='/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/testing.txt', level = 'DEBUG')

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s : %(levelname)s : %(message)s',
                    filename = '/var/www/batutaws_moodle/WebService/apps/Batuta/testing.log',
                    filemode = 'w',)

# logging.setLevel(logging.DEBUG, filename = '/home/jaop/Documentos/Desarrollo-SW/proyectobatuta/WebService//var/www/batutaws_moodle/WebService/apps/Batuta/testing.log')

# #NOTA: CONFIGURAR CONTRASEÑA DESDE ADMIMISTRADOR EN MOODLE, ACEPTE SIN RESTRICCIONES DE CARACTERES

'''-----------------------------------------CONSULTA_PERIODO_SIGE-----------------------------------'''
"""
La función periodo_sige() consulta el perido que se encuentra registrado
en SIGE y retorna el at_id de ese perido.
"""
#@csrf_exempt
def periodo_sige():
    datos = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_current_academic_terms",
        }
    )

    datos_at_id = datos["data"]["ar_current_academic_terms"]

    at_id = []      # at_id codigo del periodo academico
    name = []
    for item in datos_at_id.values():
        at_id = list(item.values())[0]
        name = list(item.values())[5]
    return at_id

'''-------------------------------------------REGISTRO TEACHERS---------------------------------------'''
#@csrf_exempt
def consulta_teachers():
    at_id = periodo_sige()
    datos = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_teachers",
        "security_branch_exclusion": 0,
        "force_search_current_branches": 0,
        "status": 1
        }
    )
    # Sacamos los datos del diccionario para poder acceder a los u_id del usuario
    datosTeacher = datos["data"]["ar_teachers"]
    # Establecemos conexion con la BD de Batuta
    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:
        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    query = "SELECT idnumber FROM mdl_user where deleted = 0"
    c.execute(query)
    myresult = c.fetchall()
    #print(myresult)
    #sacamos el número de datos que tiene sige
    cont_uid_sige = 0
    cont_uid_sige = len(datosTeacher)
    cont_uid_tabla = 0


    lista_datos_sige = []
    for item in datosTeacher.values():
        lista_datos_sige.append(item)
        u_id = list(item.values())[0]
        for i in myresult:
            # condicion cuando el u_id esta en la tabla
            if u_id in i :
                #contador (cont_uid_tabla) de los u_id en la tabla mdl_user
                cont_uid_tabla += 1


    nuevos_teachers = []
    # Validamos si el número de usuarios en sige es igual al número usuarios de la tabla mld_user (BD batuta)
    if cont_uid_sige == cont_uid_tabla:
        print("El ultimo registro teachers de sige coincide con el ultimo registro de Moodle (iguales)")

        logging.info('El ultimo registro teachers de sige coincide con el ultimo registro de Moodle (iguales)')

    # Validamos si el número de usuarios en sige es mayor al número de usuarios de la tabla mld_user (BD batuta)
    elif cont_uid_sige > cont_uid_tabla:

        #diferencia_user = cont_uid_sige - cont_uid_tabla

        print("El ultimo registro de la tabla Mdl_user es diferente al ultimo registro de la API sige.")
        logging.info('El ultimo registro de la tabla Mdl_user es diferente al ultimo registro de la API sige.')

        # ultimo_dato = lista_datos_sige[cont_uid_tabla]

        # lista nuevos usuarios teachers
        cont_i = 0
        for i in lista_datos_sige:
          cont_i += 1

          if cont_i > cont_uid_tabla:
              nuevos_teachers.append(i)

        return nuevos_teachers

"""
La función get_teachers(), inserta nuevos docentes en Moodle en la tabla 
mdl_user, con en base al u_id se realizan los filtros para verificar si el 
docente se encuentra registrado, si el usuario existe se enviará su u_id 
a la función teachers_x_courses la cual actualizará los cursos y grupos 
asociados al docente
"""
def get_teachers():

    try:
        global  time_teachers
        time_teachers = True
        try:
            con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            c = con.cursor()
        except:
            error = "ERROR CONEXION BASE DE DATOS STAGE"
            errores(error)

        # consultamos el campo u_id de la tabla mdl_user
        query = "SELECT idnumber FROM mdl_user where deleted = 0"
        c.execute(query)
        myresult = c.fetchall()

        consulta_funcion = consulta_teachers()
        print("###################################")
        respuesta = ["Registro de sige a moodle"]
        print(respuesta)
        datos_ultimo_usuario = []

        if consulta_funcion:
            for i_consulta in consulta_funcion:
                datos_ultimo_usuario.clear()
                for i in i_consulta.values():
                    datos_ultimo_usuario.append(i)

                # posicion de los items de la API sige.
                u_id = datos_ultimo_usuario[0]
                nid = datos_ultimo_usuario[1]
                login =datos_ultimo_usuario[2]
                first_name = datos_ultimo_usuario[4]
                last_name = datos_ultimo_usuario[5]
                email = datos_ultimo_usuario[6]
                b_id = datos_ultimo_usuario[15]
                # Validamos si el email llega vacio, le pasamos unn correo de prueba
                if email == None:
                    str_login =  login
                    a,b = 'áéíóúüñÁÉÍÓÚÜÑ','aeiouunAEIOUUN'
                    trans = str.maketrans(a,b)
                    new_str_login = str_login.translate(trans)
                    email = (new_str_login + "@sincorreo.com")
                # Validamos si el número de telefono llega vacio le pasamos uno de prueba.
                mobile_number = datos_ultimo_usuario[8]
                if mobile_number == None:
                    mobile_number = ('1234567890')

                cont_uid_bd_mdl_user = 0
                for i in myresult:
                    # Validamos si u_id esta en la tabla
                    if u_id in i :

                        cont_uid_bd_mdl_user += 1

                if cont_uid_bd_mdl_user>= 1:
                    teachers_x_courses(u_id,nid)
                    print("teacher ya existe")
                    logging.info('teacher %s ya existe',u_id)

                # Validamos si el usuario no existe en la base de datos Batuta y hacemos la insercion del usuario
                # con los datos que me llegan desde al API de sige.
                elif cont_uid_bd_mdl_user == 0:
                    if b_id == '0' :

                        name_b_id = "No hay informacion sede 0"
                        departamento = "No hay informacion sede 0"
                        ciudad = "No hay informacion sede 0"
                    else:
                        query = "SELECT name_bid,state,city FROM bat_sede WHERE b_id = "+str(b_id)+""
                        c.execute(query)
                        name_place_bid = c.fetchall()
                        name_b_id = name_place_bid[0][0]
                        departamento = name_place_bid[0][1]
                        ciudad = name_place_bid[0][2]

                    url = BATUTA_URL
                    #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        'wsfunction' : 'core_user_create_users',
                        'users[0][username]' : '',
                        'users[0][firstname]' : '',
                        'users[0][lastname]' : '',
                        'users[0][email]' : '',
                        'users[0][password]' : '',
                        'users[0][auth]' : 'manual',
                        'users[0][preferences][0][type]' : 'auth_forcepasswordchange',
                        'users[0][preferences][0][value]' : '1',
                        'users[0][idnumber]' : '',
                        'users[0][phone1]' : '',
                        'users[0][department]' : '',
                        'users[0][city]' : '',
                        'users[0][institution]' : '',
                        'moodlewsrestformat' : 'json'
                        }
                    args ['users[0][username]'] = nid.lower()
                    args ['users[0][firstname]'] = traducir(first_name)
                    args ['users[0][lastname]'] = traducir(last_name)
                    args ['users[0][email]'] = email
                    args ['users[0][password]'] = nid
                    args ['users[0][idnumber]'] = u_id
                    args ['users[0][phone1]'] = mobile_number
                    args ['users[0][department]'] = departamento
                    args ['users[0][city]'] = ciudad
                    args['users[0][institution]'] = name_b_id

                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    core_user_create_users('USUARIO REGISTRADO CON EXITO ' + 'NOMBRE: ' + traducir(first_name) + ' APELLIDO: ' + traducir(last_name) + ' NID: ' + nid) if 'id' in response[0].keys() else core_user_create_users('ERROR AL REGISTRAR USUARIO '+ 'NOMBRE: ' + traducir(first_name) + ' APELLIDO: ' + traducir(last_name) + ' NID: ' + str(nid))
                    time.sleep(8)

                    print("REGISTRO EXITOSAMENTE TEACHERS")
                    logging.info('REGISTRO TEACHER %s EXITOSAMENTE', nid)
                    teachers_x_courses(u_id,nid)

        else:
            print("consulta teachers vacia")
        c.close()
    except:
        error = "ERROR_GET_TEACHERS"
        errores(error)
    time_teachers = False

def teachers_x_courses(u_id,nid):

    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:

        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    query = "SELECT id FROM mdl_user where idnumber = "+str(u_id)+" and deleted = 0"
    c.execute(query)
    id_user = c.fetchall()
    print("Otro id user mas ",id_user, "  ",u_id)
    if id_user:
        cont_sin = 0
        lista_sincursos = []

        at_id = periodo_sige()
        datos = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_teaching_load",
        "u_id": u_id,
        "at_id": at_id
        }
        )
        if datos["data"]:
            datosTeacherxcourse = datos["data"]["teaching_load"]
            print("Cuantos porfes con el u_id me traigo aqui ??",len(datosTeacherxcourse))
            print(datosTeacherxcourse)
            lista_datosTeacherxcourse = []

            for item in datosTeacherxcourse.values():

                lista_datosTeacherxcourse.clear()
                for i in item.values():
                    lista_datosTeacherxcourse.append(item)
                c_id = list(item.values())[4]
                group = list(item.values())[5]

                query1 = "SELECT id FROM mdl_course where idnumber = "+str(c_id)+""
                c.execute(query1)
                id_curso = c.fetchall()
                print("Hay o no un id curso para el profe",id_curso)
                if id_curso:

                    #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                    url = BATUTA_URL
                    #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        'wsfunction' : 'enrol_manual_enrol_users',
                        'enrolments[0][roleid]' : 4,
                        'enrolments[0][userid]' : '',
                        'enrolments[0][courseid]' : '',
                        'enrolments[0][suspend]' : 0,
                        'moodlewsrestformat' : 'json'
                        }
                    args ['enrolments[0][userid]'] = id_user[0][0]
                    args ['enrolments[0][courseid]'] = id_curso[0][0]

                    print("REGISTRO MIEMBRO MANUAL EXITOSO",u_id)
                    logging.info('REGISTRO TEACHER CON NID = %s  AL CURSO %s EXITOSO',nid, c_id)

                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))
                    # print("response de los teachers ",response.text)
                else:
                    print("no hay id_curso para el teacher")
                    course_update_teachers(c_id,u_id,nid)
        else:
            print("teacher sin grupo o curso asociado")
            logging.info('profesor %s sin grupo o curso asociado',nid)
    else:
        logging.info('USUARIO %s SIN ID',nid)
        get_teachers()

def course_update_teachers(c_id,u_id,nid):
        print("Entra curso 3")
        datos = getData(
            {
            "school_api_key": SCHOOL_API_KEY,
            "user_api_key": USER_API_KEY,
            "action": "get_courses",
            "c_id": c_id,
            "at_id" : periodo_sige(),
            }
        )
        datos_cursos = datos["data"]["ar_courses"]
        try:
            con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            #con = mysql.connector.connect( host='localhost', user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            c = con.cursor()
        except:
            error = "ERROR CONEXION BASE DE DATOS STAGE"
            errores(error)

        for item in datos_cursos.values():
            c_id = list(item.values())[0]
            lv_id = list(item.values())[26]
            name =  list(item.values())[27]
            fullname = list(item.values())[33]

            datos_categoria = "SELECT id FROM mdl_course_categories WHERE idnumber = '"+str(lv_id)+"' "
            c.execute(datos_categoria)
            categoria = c.fetchall()
            category_bd = categoria[0][0]


            url = BATUTA_URL
            #args = { 'wstoken' : BATUTA_TOKEN, #stage
            args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                'wsfunction' : 'core_course_create_courses',
                'courses[0][fullname]' : '',
                'courses[0][shortname]' : '',
                'courses[0][categoryid]' : '',
                'courses[0][idnumber]' : '',
                'moodlewsrestformat' : 'json'
                }

            args ['courses[0][fullname]'] = traducir(fullname)
            args ['courses[0][shortname]'] = fullname+"_"+str(periodo_sige())
            args ['courses[0][categoryid]'] = category_bd
            args ['courses[0][idnumber]'] = c_id
            response = requests.post(url, data=args)
            response = response.json()
            print(response)
            core_course_create_courses('CURSO REGISTRADO CON EXITO '+ 'C_ID: ' + str(c_id)+ ' CATEGORIA: ' + str(category_bd)) if 'id' in response[0].keys() else core_course_create_courses('ERROR AL REGISTRAR CURSO '+ 'C_ID: ' + str(c_id) + ' CATEGORIA: ' + str(category_bd))
            id_curso = response[0]["id"]
            print("vamos a ver que ahora es id_curso ", id_curso)
            print("CURSO REGISTRADO EXITOSAMENTE")
            logging.info('CURSO %s REGISTRADO EXITOSAMENTE',c_id)
            print("imprimiendo curso idnumber para consulta ",c_id)
            
            query = "SELECT id FROM mdl_user where idnumber = "+str(u_id)+" and deleted = 0"
            c.execute(query)
            anterior_id = c.fetchall()
            id_user = anterior_id
            print("mirar respuesta de id_user", id_user)

            if id_user:
                #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------
                url = BATUTA_URL
                    #args = { 'wstoken' : BATUTA_TOKEN, #stage
                args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                    #'wstoken' : BATUTA_TOKEN,
                    'wsfunction' : 'enrol_manual_enrol_users',
                    'enrolments[0][roleid]' : 5,
                    'enrolments[0][userid]' : '',
                    'enrolments[0][courseid]' : '',
                    'enrolments[0][suspend]' : 0,
                    'moodlewsrestformat' : 'json'
                    }
                args ['enrolments[0][userid]'] = id_user[0][0]
                args ['enrolments[0][courseid]'] = id_curso

                print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                logging.info('REGISTRO TEACHER CON NID = %s  AL CURSO  %s EXITOSO',nid, c_id)
                response = requests.post(url, data=args)
                response = response.json()
                print(response)
                enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))

'''---------------------------------------------INSERCCION_NUEVAS_SEDES-------------------------------'''

def consulta_b_id(bid):
    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        #con = mysql.connector.connect( host='localhost', user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:
        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    """query = "SELECT b_id FROM bat_sede"
    c.execute(query)
    myresult = c.fetchall()"""

    datos_b_id = getData(
            {
            "school_api_key": SCHOOL_API_KEY,
            "user_api_key": USER_API_KEY,
            "action": "get_branches",
            "b_id": bid,
            }
        )
    datos_b_id = datos_b_id["data"]["ar_branches"]
    print("Aqui imprimos la lista que da errror")
    print(datos_b_id)
    time.sleep(5)
    for item in datos_b_id.values():
        b_id = list(item.values())[0]
        parent_b_id = list(item.values())[1]
        name_b_id = list(item.values())[2]
        print(type(b_id),b_id)

        query = "SELECT departamento,ciudad FROM bat_ciudad WHERE cod_sige= "+str(parent_b_id)+""
        c.execute(query)
        lugar = c.fetchall()
        if lugar:
            state = lugar[0][0]
            city = lugar[0][1]
        else:

            datos_ciudad = getData(
            {
            "school_api_key": SCHOOL_API_KEY,
            "user_api_key": USER_API_KEY,
            "action": "get_branches",
            "b_id" : parent_b_id,
            "bt_id":2
                }
            )
            print("Asi viene los datos de la ciudad")
            print(datos_ciudad)
            datos_ciudad = datos_ciudad["data"]["ar_branches"]
            for data in datos_ciudad.values():
                cod_departamento = list(data.values())[1]
                city = list(data.values())[2]
                #busquedad del departamento asociado con codigo sige
                departamento = "SELECT nombre FROM bat_departamento WHERE cod_sige= "+str(cod_departamento)+""
                c.execute(departamento)
                name_departamento = c.fetchall()
                state = name_departamento[0][0]
                # inserta departmento con la ciudad en la tabla bat_ciudad
                sql = "INSERT INTO bat_ciudad(cod_sige,departamento,ciudad) VALUES (%s,%s,%s)"
                val = (parent_b_id,state,city)
                c.execute(sql, val)
                con.commit()

        sql = "INSERT INTO bat_sede(b_id,usernumber,new_usernumber,name_bid,state,city ) VALUES (%s,%s,%s,%s,%s,%s)"
        val = (b_id,0,0,name_b_id,state,city)
        print("imprimimos valll")
        print(val)
        c.execute(sql, val)
        con.commit()
        #logging.INFO('REGISTRO DE NUEVA SEDE %s',b_id)
        time.sleep(15)

'''--------------------------------------------------INSERCCION_NUEVOS_ENROLLMENTS----------------------------------------'''

#@csrf_exempt
def insert_new_enrollments(get_enrollemnts):
    print("entramos a inseatr los nuevos enrollments ",len(get_enrollemnts))
    start = time.time()
    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:
        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    # consultamos el campo u_id de la tabla mdl_user
    query = "SELECT idnumber FROM mdl_user where deleted = 0"
    c.execute(query)
    enrollment = c.fetchall()
    # captura el valor del ultimo usuario de la funcion consulta_matricula
    respuesta = ["Registro matricula"]
    datos_ultimo_usuario = []
    print("cantidad de users ", len(enrollment))

    for i_consulta in (get_enrollemnts):
        print(i_consulta)
        datos_ultimo_usuario.clear()
        print("Que tenemos aqui")
        print(datos_ultimo_usuario)

        for i in i_consulta.values():
            datos_ultimo_usuario.append(i)

        print("DATOS LAST USER")
        print(datos_ultimo_usuario)

        e_id = i_consulta["e_id"]        
        u_id = i_consulta["u_id"]
        first_name = i_consulta["first_name"]
        last_name = i_consulta["last_name"]
        login = i_consulta["login"]
        nid = i_consulta["nid"]
        nid= nid.lower()
        bid = i_consulta["b_id"]
        print(first_name,last_name,"Este es el user que quiere")
        print(bid)
        #time.sleep(3)

        query = "SELECT name_bid,state,city FROM bat_sede WHERE b_id = "+str(bid)+""
        c.execute(query)
        name_place = c.fetchall()

        if not name_place:
            consulta_b_id(bid)
        else:
            name_b_id = name_place[0][0]
            departamento = name_place[0][1]
            ciudad = name_place[0][2]
            # Validamos correo, porque entre los datos los usuarios no traen correo ni phone
            str_login =  login
            a,b = 'áéíóúüñÁÉÍÓÚÜÑ','aeiouunAEIOUUN'
            trans = str.maketrans(a,b)
            new_str_login = str_login.translate(trans)
            email = (new_str_login + "@sincorreo.com")
            cont_uid_bd_mdl_enrollment = 0

            for i in enrollment:
                if u_id in i :  # Validamos si idnumber esta en la tabla mdl_user
                    cont_uid_bd_mdl_enrollment += 1

            if cont_uid_bd_mdl_enrollment >=1:
                print("USUARIO YA REGISTRADO ENROLLMENTS")
                logging.info('USUARIO %s SE ENCONTRABA REGISTRADO EN LA PLATAFORMA', nid)

                query = "SELECT id,phone2, username FROM mdl_user WHERE idnumber = "+str(u_id)+" and deleted = 0"
                c.execute(query)
                dato = c.fetchall()
                id = dato[0][0]
                eid_bd = dato[0][1]
                username_bd = dato[0][2]

                print("datos en base del user")
                print(id,eid_bd,username_bd,nid)

                if eid_bd != e_id:
                    if  username_bd != nid:
                        print("actualiza el nid aqui")
                        logging.info('CAMBIO DE NID USUARIO %s REGISTRADO EN LA PLATAFORMA', nid)

                        url = BATUTA_URL
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_user_update_users',
                            'users[0][id]' : '',
                            'users[0][username]' : '',
                            'users[0][firstname]' : '',
                            'users[0][lastname]' : '',
                            'users[0][phone2]' : '',
                            'moodlewsrestformat' : 'json'
                        }
                        args ['users[0][id]'] = id
                        args ['users[0][username]'] = nid.lower()
                        args ['users[0][firstname]'] = traducir(first_name)
                        args ['users[0][lastname]'] = traducir(last_name)
                        args ['users[0][phone2]'] = e_id
                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        enrol_manual_enrol_users('ERROR EN ACTUALIZAR USUARIO ' + str(nid)) if response != None else enrol_manual_enrol_users('USUARIO ACTUALIZADO EXITOSAMENTE ' + str(nid))
                        print("aqui")
                        print(args)

                    else:
                        print("solo cambia el e_id aqui")
                        logging.info('CAMBIO DE E_ID USUARIO %s REGISTRADO EN LA PLATAFORMA', nid)

                        url = BATUTA_URL
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_user_update_users',
                            'users[0][id]' : '',
                            'users[0][phone2]' : '',
                            'moodlewsrestformat' : 'json'
                        }
                        args ['users[0][id]'] = id
                        args ['users[0][phone2]'] = e_id
                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_user_create_users('SE CAMBIO EL E_ID DEL USUARIO ' + str(nid)) if not response  else core_user_create_users('ERROR AL CAMBIAR E_ID DEL USUARIO ' + str(nid))
                        print("Y que pasa con el response")
                        print(response)

                    insert_new_group_members(e_id,nid)
                else:
                    insert_new_group_members(e_id,nid)

            elif cont_uid_bd_mdl_enrollment == 0:
                print("creando un nuevo user ")
                url = BATUTA_URL
                args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                    'wsfunction' : 'core_user_create_users',
                    'users[0][username]' : '',
                    'users[0][firstname]' : '',
                    'users[0][lastname]' : '',
                    'users[0][email]' : '',
                    'users[0][password]' : '',
                    'users[0][auth]' : 'manual',
                    'users[0][preferences][0][type]' : 'auth_forcepasswordchange',
                    'users[0][preferences][0][value]' : '1',
                    'users[0][phone2]' : '',
                    'users[0][department]' : '',
                    'users[0][city]' : '',
                    'users[0][institution]' : '',
                    'moodlewsrestformat' : 'json'
                    }

                args ['users[0][username]'] = nid.lower()
                args ['users[0][firstname]'] = traducir(first_name)
                args ['users[0][lastname]'] = traducir(last_name)
                args ['users[0][email]'] = email
                args ['users[0][password]'] = nid
                args ['users[0][idnumber]'] = u_id
                args ['users[0][phone2]'] = e_id
                args ['users[0][department]'] = departamento
                args ['users[0][city]'] = ciudad
                args['users[0][institution]'] = name_b_id

                response = requests.post(url, data=args)
                response = response.json()
                print(response)
                core_user_create_users('USUARIO REGISTRADO CON EXITO ' + 'NOMBRE: ' + traducir(first_name) + ' APELLIDO: ' + traducir(last_name) + ' NID: ' + str(nid)) if 'id' in response[0].keys() else core_user_create_users('ERROR AL REGISTRAR USUARIO '+ 'NOMBRE: ' + traducir(first_name) + ' APELLIDO: ' + traducir(last_name) + ' NID: ' + str(nid))

                print("REGISTRO EXITOSAMENTE ENROLLMENTS")
                logging.info('REGISTRO  USUARIO NID = %s EXITOSAMENTE EN LA PLATAFORMA', nid)
                time.sleep(3)
                end = time.time()
                print(end - start)
                print("--------- get_b_id_matriculando",u_id)
                insert_new_group_members(e_id,nid)
    c.close()
    # except:
    #     error = "ERROR_INSERT_ENROLLMENT"
    #     errores(error)
    print("Esperamos varios segundos(Salida de enrollments)")
    #time.sleep(8)

def insert_new_group_members(e_id,nid):
    print("entra a insert group")
    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:
        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    datos_group_sige = getData(
                            {
                            "school_api_key": SCHOOL_API_KEY,
                            "user_api_key": USER_API_KEY,
                            "action": "get_groups_x_course_x_enrollment",
                            "e_id" : e_id,
                            "ask_for_e_id" : 0,
                            "index" : "c_id"
                            }
                        )

    print("Aqui traemos grupos de sige")
    print(datos_group_sige)

    if datos_group_sige["data"]:
        datos_group = datos_group_sige["data"]["ar_groups_x_course_x_enrollment"]
        datos_groupss = []
        for i in datos_group.values():
            datos_groupss.clear()
            for data in i.values():
                datos_groupss.append(data)

            c_id = datos_groupss[1]
            group = datos_groupss[2]

            query1 = "SELECT id FROM mdl_course where idnumber = "+str(c_id)+""
            c.execute(query1)
            id_curso = c.fetchall()
            print("imprimimos el id_curso ",id_curso)

            if id_curso:
                query2 = "SELECT id FROM mdl_groups where  courseid = "+str(id_curso[0][0])+" and name = '"+group+"'"
                c.execute(query2)
                id_grupo = c.fetchall()
                print("Imprimimos id_grupo")
                print(id_grupo)

                cont_id_grupo = 0
                if id_grupo:
                    cont_id_grupo += 1

                if cont_id_grupo >=1:

                    print("CURSO EN GRUPO YA SE ENCUENTRA REGISTRADO")
                    logging.info('CURSO %s EN GRUPO REGISTRADO',c_id)

                    query = "SELECT id FROM mdl_user where phone2 = "+str(e_id)+" and deleted = 0"
                    c.execute(query)
                    anterior_id = c.fetchall()
                    id_user = anterior_id

                    # print("#########################")
                    # print(id_curso)
                    # print(id_user)
                    if id_user:
                        #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------
                        url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            #'wstoken' : BATUTA_TOKEN,
                            'wsfunction' : 'enrol_manual_enrol_users',
                            'enrolments[0][roleid]' : 5,
                            'enrolments[0][userid]' : '',
                            'enrolments[0][courseid]' : '',
                            'enrolments[0][suspend]' : 0,
                            'moodlewsrestformat' : 'json'
                            }
                        args ['enrolments[0][userid]'] = id_user[0][0]
                        args ['enrolments[0][courseid]'] = id_curso[0][0]

                        print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                        logging.info('REGISTRO USUARIO NID= %s AL CURSO %s EXITOSO',nid,c_id)
                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))

                        #------------------------INSERCION MIEMBROS DEL GRUPO----------------

                        url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_group_add_group_members',
                            'members[0][groupid]' : '',
                            'members[0][userid]' : '',
                            'moodlewsrestformat' : 'json'
                            }
                        args ['members[0][groupid]'] = id_grupo[0][0]
                        args ['members[0][userid]'] = id_user[0][0]

                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_group_add_group_members('ERROR AL AGREGAR USER ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(id_grupo[0][0])) if response != None else core_group_add_group_members('USUARIO REGISTRADO CON EXITO ' + 'NID: ' + str(nid) + ' AL GRUPO: '+ str(id_grupo[0][0]))
                        print("REGISTRO MIEMBROS  ENROLLMENTS DE GRUPO EXITOSO")
                        logging.info('REGISTRO USUARIO NID = %s, EN GRUPO %s EXITOSO', nid,group)

                elif cont_id_grupo == 0:
                    query1 = "SELECT fullname FROM mdl_course where id = "+str(id_curso[0][0])+""
                    c.execute(query1)
                    name_curso = c.fetchall()
                    name_curso = name_curso[0][0]
                    name_curso1 = str("curso asociado:"+name_curso)

                    #INSERCION GRUPO
                    #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                    url = BATUTA_URL
                    #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        'wsfunction' : 'core_group_create_groups',
                        'groups[0][courseid]' : '',
                        'groups[0][name]' : '',
                        'groups[0][description]' : '',
                        'moodlewsrestformat' : 'json'
                        }

                    args ['groups[0][courseid]'] = id_curso[0][0]
                    args ['groups[0][name]'] = group
                    args ['groups[0][description]'] = name_curso1
                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    core_group_create_groups('REGISTRO CURSO EN GRUPO EXITOSO ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]) + ' GRUPO: ' + group) if 'id' in response[0].keys() else core_group_create_groups('ERROR EN REGISTRO CURSO EN GRUPO ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]) + ' GRUPO: ' + group)
                    print("AQui tenemos response de creacion de curso")
                    id_grupo = response[0]["id"]
                    print("REGISTRO CURSO EN GRUPO EXITOSO")
                    logging.info('REGISTRO  GRUPO %s EN CURSO c_id = %s EXITOSO',group,c_id)
                    time.sleep(15)

                    query = "SELECT id FROM mdl_user where phone2 = "+e_id+" and deleted = 0"
                    c.execute(query)
                    anterior_id = c.fetchall()
                    id_user = anterior_id

                    if id_user:

                        #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------

                        url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            #'wstoken' : BATUTA_TOKEN,
                            'wsfunction' : 'enrol_manual_enrol_users',
                            'enrolments[0][roleid]' : 5,
                            'enrolments[0][userid]' : '',
                            'enrolments[0][courseid]' : '',
                            'enrolments[0][suspend]' : 0,
                            'moodlewsrestformat' : 'json'
                            }
                        args ['enrolments[0][userid]'] = id_user[0][0]
                        args ['enrolments[0][courseid]'] = id_curso[0][0]

                        print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                        logging.info('REGISTRO USUARIO NID = %s , AL CURSO %s EXITOSO',nid,c_id)

                        response = requests.post(url, data=args)
                        response = response.json()
                        print("este es el response de error")
                        print(response)
                        enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))
                        print("y los args ", args)
                        print("Aqui el response de registro de user a curso",nid,c_id)
                        print("Estos son los datos que mandamos al query ",id_curso[0][0] ,group)

                        #------------------------INSERCION MIEMBROS DEL GRUPO----------------
                        """query3 = "SELECT id FROM mdl_groups where  courseid = "+str(id_curso[0][0])+" and name = '"+group+"'"
                        c.execute(query3)
                        id_grupo = c.fetchall()"""
                        print("respuesta de BD con los grupos",id_grupo)
                        #Cambie id group por id_grupo es el mismo
                        new_id_group = id_grupo

                        if new_id_group:
                            #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                            url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                            args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                                'wsfunction' : 'core_group_add_group_members',
                                'members[0][groupid]' : '',
                                'members[0][userid]' : '',
                                'moodlewsrestformat' : 'json'
                                }
                            args ['members[0][groupid]'] = new_id_group
                            args ['members[0][userid]'] = id_user

                            response = requests.post(url, data=args)
                            response = response.json()
                            print(response)
                            core_group_add_group_members('ERROR EN AGREGAR USUARIO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group)) if response != None else core_group_add_group_members('SE AGREGO USUARIO CON EXITO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group))
                            print("REGISTRO MIEMBROS ENROLLMENTS DE GRUPO EXITOSO")
                            logging.info('REGISTRO USUARIO NID = %s, EN GRUPO %s EXITOSO',nid,group)
            else:
                course_new_update(c_id,e_id,group,nid)

def course_new_update(c_id,e_id,group,nid):
        print("Entra curso 2")
        datos = getData(
            {
            "school_api_key": SCHOOL_API_KEY,
            "user_api_key": USER_API_KEY,
            "action": "get_courses",
            "c_id": c_id,
            "at_id" : periodo_sige(),

            }
        )
        datos_cursos = datos["data"]["ar_courses"]
        try:
            con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            c = con.cursor()
        except:
            error = "ERROR CONEXION BASE DE DATOS STAGE"
            errores(error)

        print("Aqui tenemos de datos del curso from SIGE")
        print(datos_cursos)

        for item in datos_cursos.values():
            c_id = list(item.values())[0]
            lv_id = list(item.values())[26]
            name =  list(item.values())[27]
            fullname = list(item.values())[33]

            datos_categoria = "SELECT id FROM mdl_course_categories WHERE idnumber = '"+str(lv_id)+"' "
            c.execute(datos_categoria)
            categoria = c.fetchall()
            category_bd = categoria[0][0]

            url = BATUTA_URL
            #args = { 'wstoken' : BATUTA_TOKEN, #stage
            args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                'wsfunction' : 'core_course_create_courses',
                'courses[0][fullname]' : '',
                'courses[0][shortname]' : '',
                'courses[0][categoryid]' : '',
                'courses[0][idnumber]' : '',
                'moodlewsrestformat' : 'json'
                }

            args ['courses[0][fullname]'] = traducir(fullname)
            args ['courses[0][shortname]'] = fullname+"_"+str(periodo_sige())
            args ['courses[0][categoryid]'] = category_bd
            args ['courses[0][idnumber]'] = c_id
            response = requests.post(url, data=args)
            response = response.json()
            print(response)
            core_course_create_courses('CURSO REGISTRADO CON EXITO '+ 'C_ID: ' + str(c_id) + ' CATEGORIA: ' + str(category_bd)) if 'id' in response[0].keys() else core_course_create_courses('ERROR AL REGISTRAR CURSO ' + 'C_ID: ' + str(c_id) + ' CATEGORIA: ' + str(category_bd))
            print("vemos que son args ",args)
            id_curso = response[0]["id"]
            print("CURSO REGISTRADO EXITOSAMENTE")
            logging.info('CURSO %s REGISTRADO EXITOSAMENTE',c_id)
            time.sleep(10)

            print("Que será id_curso")
            print(id_curso)

            query2 = "SELECT id FROM mdl_groups where  courseid = "+str(id_curso)+" and name = '"+group+"'"
            c.execute(query2)
            id_grupo = c.fetchall()
            print("cual es el id grupo",id_grupo)

            cont_id_grupo = 0
            if id_grupo:
                cont_id_grupo += 1

            if cont_id_grupo >=1:

                print("CURSO EN GRUPO YA SE ENCUENTRA REGISTRADO")
                logging.info('CURSO %s EN GRUPO REGISTRADO',c_id)

                query = "SELECT id FROM mdl_user where phone2 = "+str(e_id)+" and deleted = 0"
                c.execute(query)
                anterior_id = c.fetchall()
                id_user = anterior_id

                if id_user:
                    #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------
                    url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        #'wstoken' : BATUTA_TOKEN,
                        'wsfunction' : 'enrol_manual_enrol_users',
                        'enrolments[0][roleid]' : 5,
                        'enrolments[0][userid]' : '',
                        'enrolments[0][courseid]' : '',
                        'enrolments[0][suspend]' : 0,
                        'moodlewsrestformat' : 'json'
                        }
                    args ['enrolments[0][userid]'] = id_user[0][0]
                    args ['enrolments[0][courseid]'] = id_curso

                    print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                    logging.info('REGISTRO USUARIO NID = %s EN CURSO %s EXITOSO',nid,c_id)
                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))

                    #------------------------INSERCION MIEMBROS DEL GRUPO----------------

                    url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        'wsfunction' : 'core_group_add_group_members',
                        'members[0][groupid]' : '',
                        'members[0][userid]' : '',
                        'moodlewsrestformat' : 'json'
                        }
                    args ['members[0][groupid]'] = id_grupo[0][0]
                    args ['members[0][userid]'] = id_user[0][0]

                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    core_group_add_group_members('ERROR EN AGREGAR USUARIO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group)) if response != None else core_group_add_group_members('SE AGREGO USUARIO CON EXITO ' +'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group))
                    print("REGISTRO MIEMBROS  ENROLLMENTS DE GRUPO EXITOSO")
                    logging.info('REGISTRO  USUARIO NID = %s, EN GRUPO %s EXITOSO', nid, group)

            elif cont_id_grupo == 0:


                name_curso1 = str("curso asociado:"+fullname)

                #INSERCION GRUPO
                #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                url = BATUTA_URL
                #args = { 'wstoken' : BATUTA_TOKEN, #stage
                args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                    'wsfunction' : 'core_group_create_groups',
                    'groups[0][courseid]' : '',
                    'groups[0][name]' : '',
                    'groups[0][description]' : '',
                    'moodlewsrestformat' : 'json',
                    }
                args ['groups[0][courseid]'] = id_curso
                args ['groups[0][name]'] = group
                args ['groups[0][description]'] = name_curso1
                response = requests.post(url, data=args)
                response = response.json()
                print(response)
                core_group_create_groups('REGISTRO CURSO EN GRUPO EXITOSO ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso) + ' GRUPO: ' + group) if 'id' in response[0].keys() else core_group_create_groups('ERROR EN REGISTRO CURSO EN GRUPO ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso) + ' GRUPO ' + group)
                id_grupo = response[0]["id"]
                print("REGISTRO CURSO EN GRUPO EXITOSO")
                logging.info('REGISTRO GRUPO  %s EN CURSO c_id = %s EXITOSO',c_id)

                query = "SELECT id FROM mdl_user where phone2 = "+str(e_id)+" and deleted = 0"
                c.execute(query)
                anterior_id = c.fetchall()
                id_user = anterior_id
                print("Sera que si esta el id user ", id_user)

                if id_user:

                    #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------

                    url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        #'wstoken' : BATUTA_TOKEN,
                        'wsfunction' : 'enrol_manual_enrol_users',
                        'enrolments[0][roleid]' : 5,
                        'enrolments[0][userid]' : '',
                        'enrolments[0][courseid]' : '',
                        'enrolments[0][suspend]' : 0,
                        'moodlewsrestformat' : 'json',
                        }
                    args ['enrolments[0][userid]'] = id_user[0][0]
                    args ['enrolments[0][courseid]'] = id_curso

                    print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                    logging.info('REGISTRO USUARIO NID = %s, AL CURSO %s EXITOSO',nid,c_id)

                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso)) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso))
                    #------------------------INSERCION MIEMBROS DEL GRUPO----------------

                    query2 = "SELECT id FROM mdl_groups where  courseid = "+str(id_curso)+" and name = '"+group+"'"
                    c.execute(query2)
                    id_group = c.fetchall()
                    new_id_group = id_grupo

                    print(" traemos aqui el id group ",new_id_group)

                    if new_id_group:

                        #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                        url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_group_add_group_members',
                            'members[0][groupid]' : '',
                            'members[0][userid]' : '',
                            'moodlewsrestformat' : 'json',
                            }
                        args ['members[0][groupid]'] = new_id_group
                        args ['members[0][userid]'] = id_user[0][0]

                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_group_add_group_members('ERROR EN AGREGAR USUARIO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group)) if response != None else core_group_add_group_members('SE AGREGO USUARIO CON EXITO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group))
                        print("REGISTRO MIEMBROS ENROLLMENTS DE GRUPO EXITOSO")
                        logging.info('REGISTRO USUARIO NID = %s, EN GRUPO %s EXITOSO',nid,group)

'''--------------------------------------------------INSERCCION_ACTUALIZACION_ENROLLMENTS----------------------------------------'''

#@csrf_exempt
def insert_update_enrollments(get_enrollemnts):
    start = time.time()
    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:

        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    # consultamos el campo u_id de la tabla mdl_user
    query = "SELECT idnumber FROM mdl_user where deleted = 0"
    c.execute(query)
    enrollment = c.fetchall()

    # captura el valor del ultimo usuario de la funcion consulta_matricula

    respuesta = ["Registro matricula"]
    datos_ultimo_usuario = []

    for i_consulta in (get_enrollemnts):
        datos_ultimo_usuario.clear()
        e_id = i_consulta["e_id"]        
        u_id = i_consulta["u_id"]
        first_name = i_consulta["first_name"]
        last_name = i_consulta["last_name"]
        login = i_consulta["login"]
        nid = i_consulta["nid"]
        nid= nid.lower()
        bid = i_consulta["b_id"]

        query = "SELECT name_bid,state,city FROM bat_sede WHERE b_id = "+str(bid)+""
        c.execute(query)
        name_place = c.fetchall()

        if not name_place:
            consulta_b_id(bid)
        else:
            name_b_id = name_place[0][0]
            departamento = name_place[0][1]
            ciudad = name_place[0][2]
            # Validamos correo, porque entre los datos los usuarios no traen correo ni phone
            str_login =  login
            a,b = 'áéíóúüñÁÉÍÓÚÜÑ','aeiouunAEIOUUN'
            trans = str.maketrans(a,b)
            new_str_login = str_login.translate(trans)
            email = (new_str_login + "@sincorreo.com")
            cont_uid_bd_mdl_enrollment = 0

            for i in enrollment:
                if u_id in i :  # Validamos si idnumber esta en la tabla mdl_user
                    cont_uid_bd_mdl_enrollment += 1

            if cont_uid_bd_mdl_enrollment >=1:
                print("USUARIO REGISTRADO ENROLLMENTS")
                logging.info('USUARIO %s SE ENCONTRABA REGISTRADO EN LA PLATAFORMA', nid)

                query = "SELECT id,phone2, username FROM mdl_user WHERE idnumber = "+str(u_id)+" and deleted = 0"
                c.execute(query)
                dato = c.fetchall()
                id = dato[0][0]
                eid_bd = dato[0][1]
                username_bd = dato[0][2]

                if eid_bd != e_id:
                    if  username_bd != nid:
                        logging.info('CAMBIO DE NID USUARIO %s REGISTRADO EN LA PLATAFORMA', nid)
                        url = BATUTA_URL
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_user_update_users',
                            'users[0][id]' : '',
                            'users[0][username]' : '',
                            'users[0][firstname]' : '',
                            'users[0][lastname]' : '',
                            'users[0][phone2]' : '',
                            'moodlewsrestformat' : 'json',
                        }
                        args ['users[0][id]'] = id
                        args ['users[0][username]'] = nid.lower()
                        args ['users[0][firstname]'] = traducir(first_name)
                        args ['users[0][lastname]'] = traducir(last_name)
                        args ['users[0][phone2]'] = e_id
                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_user_create_users('ERROR EN ACTUALIZAR USUARIO ' + str(nid)) if response != None else enrol_manual_enrol_users('USUARIO ACTUALIZADO EXITOSAMENTE ' + nid)

                    else:
                        logging.info('CAMBIO DE E_ID USUARIO %s REGISTRADO EN LA PLATAFORMA', nid)

                        url = BATUTA_URL
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_user_update_users',
                            'users[0][id]' : '',
                            'users[0][phone2]' : '',
                            'moodlewsrestformat' : 'json',
                        }
                        args ['users[0][id]'] = id
                        args ['users[0][phone2]'] = e_id
                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_user_create_users('SE CAMBIO EL E_ID DEL USUARIO ' + str(nid)) if 'id' in response[0].keys() else core_user_create_users('ERROR AL CAMBIAR E_ID DEL USUARIO ' + str(nid))

                    insert_update_group_members(e_id,nid)

                else:
                    insert_update_group_members(e_id,nid)

            elif cont_uid_bd_mdl_enrollment == 0:

                url = BATUTA_URL
                args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                    'wsfunction' : 'core_user_create_users',
                    'users[0][username]' : '',
                    'users[0][firstname]' : '',
                    'users[0][lastname]' : '',
                    'users[0][email]' : '',
                    'users[0][password]' : '',
                    'users[0][auth]' : 'manual',
                    'users[0][preferences][0][type]' : 'auth_forcepasswordchange',
                    'users[0][preferences][0][value]' : '1',
                    'users[0][phone2]' : '',
                    'users[0][department]' : '',
                    'users[0][city]' : '',
                    'users[0][institution]' : '',
                    'moodlewsrestformat' : 'json',
                    }
                args ['users[0][username]'] = nid.lower()
                args ['users[0][firstname]'] = traducir(first_name)
                args ['users[0][lastname]'] = traducir(last_name)
                args ['users[0][email]'] = email
                args ['users[0][password]'] = nid
                args ['users[0][idnumber]'] = u_id
                args ['users[0][phone2]'] = e_id
                args ['users[0][department]'] = departamento
                args ['users[0][city]'] = ciudad
                args['users[0][institution]'] = name_b_id

                response = requests.post(url, data=args)
                response = response.json()
                print(response)
                core_user_create_users('REGISTRO EXITOSAMENTE ENROLLMENTS '+ traducir(first_name) + ' APELLIDO: ' + traducir(last_name) + 'NID: ' + str(nid)) if 'id' in response[0].keys() else core_user_create_users('ERROR AL REGISTRAR ENROLLMENTS '+ traducir(first_name) + ' APELLIDO: ' + traducir(last_name) + ' NID: ' + str(nid))

                print("REGISTRO EXITOSAMENTE ENROLLMENTS")
                logging.info('REGISTRO  USUARIO NID = %s EXITOSAMENTE EN LA PLATAFORMA', nid)
                time.sleep(3)
                end = time.time()
                print(end - start)
                print("--------- get_b_id_matriculando",u_id)
                insert_update_group_members(e_id,nid)
    c.close()
    # except:
    #     error = "ERROR_INSERT_ENROLLMENT"
    #     errores(error)

def insert_update_group_members(e_id,nid):

    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:
        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    datos_group_sige = getData(
                            {
                            "school_api_key": SCHOOL_API_KEY,
                            "user_api_key": USER_API_KEY,
                            "action": "get_groups_x_course_x_enrollment",
                            "e_id" : e_id,
                            "ask_for_e_id" : 0,
                            "index" : "c_id"
                            }
                        )

    if datos_group_sige["data"]:
        datos_group = datos_group_sige["data"]["ar_groups_x_course_x_enrollment"]
        datos_groupss = []
        for i in datos_group.values():
            datos_groupss.clear()
            for data in i.values():
                datos_groupss.append(data)

            c_id = datos_groupss[1]
            group = datos_groupss[2]

            print("CYG ",c_id,"  ",group)
            query1 = "SELECT id FROM mdl_course where idnumber = "+str(c_id)+""
            c.execute(query1)
            id_curso = c.fetchall()
            print("TEnemos si o no el id _curso ",id_curso)

            if id_curso:

                query2 = "SELECT id FROM mdl_groups where  courseid = "+str(id_curso[0][0])+" and name = '"+group+"'"
                c.execute(query2)
                id_grupo = c.fetchall()

                cont_id_grupo = 0
                if id_grupo:
                    cont_id_grupo += 1

                if cont_id_grupo >=1:

                    print("CURSO EN GRUPO YA SE ENCUENTRA REGISTRADO")
                    logging.info('CURSO %s EN GRUPO REGISTRADO',c_id)

                    query = "SELECT id FROM mdl_user where phone2 = "+str(e_id)+" and deleted = 0"
                    c.execute(query)
                    anterior_id = c.fetchall()
                    id_user = anterior_id

                    if id_user:
                        #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------
                        url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            #'wstoken' : BATUTA_TOKEN,
                            'wsfunction' : 'enrol_manual_enrol_users',
                            'enrolments[0][roleid]' : 5,
                            'enrolments[0][userid]' : '',
                            'enrolments[0][courseid]' : '',
                            'enrolments[0][suspend]' : 0,
                            'moodlewsrestformat' : 'json',
                            }
                        args ['enrolments[0][userid]'] = id_user[0][0]
                        args ['enrolments[0][courseid]'] = id_curso[0][0]

                        print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                        logging.info('REGISTRO USUARIO NID= %s AL CURSO %s EXITOSO',nid,c_id)
                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))

                        #------------------------INSERCION MIEMBROS DEL GRUPO----------------

                        url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_group_add_group_members',
                            'members[0][groupid]' : '',
                            'members[0][userid]' : '',
                            'moodlewsrestformat' : 'json',
                            }
                        args ['members[0][groupid]'] = id_grupo[0][0]
                        args ['members[0][userid]'] = id_user[0][0]

                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_group_add_group_members('ERROR EN AGREGAR USUARIO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group)) if response != None  else core_group_add_group_members('SE AGREGO USUARIO CON EXITO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group))
                        print("REGISTRO MIEMBROS  ENROLLMENTS DE GRUPO EXITOSO")
                        logging.info('REGISTRO USUARIO NID = %s, EN GRUPO %s EXITOSO', nid,group)

                elif cont_id_grupo == 0:

                    query1 = "SELECT fullname FROM mdl_course where id = "+str(id_curso[0][0])+""
                    c.execute(query1)
                    name_curso = c.fetchall()
                    name_curso = name_curso[0][0]
                    name_curso1 = str("curso asociado:"+name_curso)

                    #INSERCION GRUPO
                    #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                    url = BATUTA_URL
                    #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        'wsfunction' : 'core_group_create_groups',
                        'groups[0][courseid]' : '',
                        'groups[0][name]' : '',
                        'groups[0][description]' : '',
                        'moodlewsrestformat' : 'json',
                        }

                    args ['groups[0][courseid]'] = id_curso[0][0]
                    args ['groups[0][name]'] = group
                    args ['groups[0][description]'] = name_curso1
                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    core_group_create_groups('REGISTRO CURSO EN GRUPO EXITOSO ' + 'NID: ' + nid + ' CURSO: ' + str(id_curso[0][0]) + ' GRUPO: ' + group) if 'id' in response[0].keys() else core_group_create_groups('ERROR EN REGISTRO CURSO EN GRUPO ' + 'NID: ' + nid + ' CURSO: ' + str(id_curso[0][0]) + ' GRUPO: ' + group)
                    id_group = response[0]["id"]
                    print("REGISTRO CURSO EN GRUPO EXITOSO")
                    logging.info('REGISTRO  GRUPO %s EN CURSO c_id = %s EXITOSO',group,c_id)
                    time.sleep(15)

                    query = "SELECT id FROM mdl_user where phone2 = "+str(e_id)+" and deleted = 0"
                    c.execute(query)
                    anterior_id = c.fetchall()
                    id_user = anterior_id

                    if id_user:

                        #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------

                        url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            #'wstoken' : BATUTA_TOKEN,
                            'wsfunction' : 'enrol_manual_enrol_users',
                            'enrolments[0][roleid]' : 5,
                            'enrolments[0][userid]' : '',
                            'enrolments[0][courseid]' : '',
                            'enrolments[0][suspend]' : 0,
                            'moodlewsrestformat' : 'json',
                            }
                        args ['enrolments[0][userid]'] = id_user[0][0]
                        args ['enrolments[0][courseid]'] = id_curso[0][0]

                        print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                        logging.info('REGISTRO USUARIO NID = %s , AL CURSO %s EXITOSO',nid,c_id)

                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))

                        #------------------------INSERCION MIEMBROS DEL GRUPO----------------
                       
                        new_id_group =id_group

                        if new_id_group:

                            #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                            url = BATUTA_URL
                            #args = { 'wstoken' : BATUTA_TOKEN, #stage
                            args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                                'wsfunction' : 'core_group_add_group_members',
                                'members[0][groupid]' : '',
                                'members[0][userid]' : '',
                                'moodlewsrestformat' : 'json',
                                }
                            args ['members[0][groupid]'] = new_id_group
                            args ['members[0][userid]'] = id_user

                            response = requests.post(url, data=args)
                            response = response.json()
                            print(response)
                            core_group_add_group_members('ERROR EN AGREGAR USUARIO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group)) if response != None else core_group_add_group_members('SE AGREGO USUARIO CON EXITO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group))
                            print("REGISTRO MIEMBROS ENROLLMENTS DE GRUPO EXITOSO")
                            logging.info('REGISTRO USUARIO NID = %s, EN GRUPO %s EXITOSO',nid,group)
            else:
                course_update(c_id,e_id,group,nid)

def course_update(c_id,e_id,group,nid):
        print("Entra curso 1")
        datos = getData(
            {
            "school_api_key": SCHOOL_API_KEY,
            "user_api_key": USER_API_KEY,
            "action": "get_courses",
            "c_id": c_id,
            "at_id" : periodo_sige(),

            }
        )
        datos_cursos = datos["data"]["ar_courses"]
        try:
            con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            c = con.cursor()
        except:
            error = "ERROR CONEXION BASE DE DATOS STAGE"
            errores(error)

        for item in datos_cursos.values():

            c_id = list(item.values())[0]
            lv_id = list(item.values())[26]
            name =  list(item.values())[27]
            fullname = list(item.values())[33]

            datos_categoria = "SELECT id FROM mdl_course_categories WHERE idnumber = '"+str(lv_id)+"' "
            c.execute(datos_categoria)
            categoria = c.fetchall()
            category_bd = categoria[0][0]

            url = BATUTA_URL
            #args = { 'wstoken' : BATUTA_TOKEN, #stage
            args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                'wsfunction' : 'core_course_create_courses',
                'courses[0][fullname]' : '',
                'courses[0][shortname]' : '',
                'courses[0][categoryid]' : '',
                'courses[0][idnumber]' : '',
                'moodlewsrestformat' : 'json'
                }

            args ['courses[0][fullname]'] = traducir(fullname)
            args ['courses[0][shortname]'] = fullname+"_"+str(periodo_sige())
            args ['courses[0][categoryid]'] = category_bd
            args ['courses[0][idnumber]'] = c_id
            response = requests.post(url, data=args)
            response = response.json()
            print(response)
            core_course_create_courses('CURSO REGISTRADO CON EXITO ' + 'C_ID: ' + str(c_id) + ' CATEGORIA: ' + str(category_bd)) if 'id' in response[0].keys() else core_course_create_courses('ERROR AL REGISTRAR CURSO ' + 'C_ID: ' + str(c_id) + ' CATEGORIA: ' + str(category_bd))
            id_curso = response[0]["id"]
            print("CURSO REGISTRADO EXITOSAMENTE")
            logging.info('CURSO %s REGISTRADO EXITOSAMENTE',c_id)
            time.sleep(15)

            """query1 = "SELECT id FROM mdl_course where idnumber = "+c_id+""
            c.execute(query1)
            id_curso = c.fetchall()"""

            query2 = "SELECT id FROM mdl_groups where  courseid = "+str(id_curso)+" and name = '"+group+"'"
            c.execute(query2)
            id_grupo = c.fetchall()

            cont_id_grupo = 0
            if id_grupo:
                cont_id_grupo += 1

            if cont_id_grupo >=1:

                print("CURSO EN GRUPO YA SE ENCUENTRA REGISTRADO")
                logging.info('CURSO %s EN GRUPO REGISTRADO',c_id)

                query = "SELECT id FROM mdl_user where phone2 = "+str(e_id)+" and deleted = 0"
                c.execute(query)
                anterior_id = c.fetchall()
                id_user = anterior_id
                if id_user:
                    #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------
                    url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        #'wstoken' : BATUTA_TOKEN,
                        'wsfunction' : 'enrol_manual_enrol_users',
                        'enrolments[0][roleid]' : 5,
                        'enrolments[0][userid]' : '',
                        'enrolments[0][courseid]' : '',
                        'enrolments[0][suspend]' : 0,
                        'moodlewsrestformat' : 'json'
                        }
                    args ['enrolments[0][userid]'] = id_user[0][0]
                    args ['enrolments[0][courseid]'] = id_curso

                    print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                    logging.info('REGISTRO USUARIO NID = %s EN CURSO %s EXITOSO',nid,c_id)
                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]))

                    #------------------------INSERCION MIEMBROS DEL GRUPO----------------

                    url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        'wsfunction' : 'core_group_add_group_members',
                        'members[0][groupid]' : '',
                        'members[0][userid]' : '',
                        'moodlewsrestformat' : 'json'
                        }
                    args ['members[0][groupid]'] = id_grupo[0][0]
                    args ['members[0][userid]'] = id_user[0][0]

                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    core_group_add_group_members('ERROR EN AGREGAR USUARIO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group)) if response != None else core_group_add_group_members('SE AGREGO USUARIO CON EXITO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group))
                    print("REGISTRO MIEMBROS  ENROLLMENTS DE GRUPO EXITOSO")
                    logging.info('REGISTRO  USUARIO NID = %s, EN GRUPO %s EXITOSO', nid, group)

            elif cont_id_grupo == 0:

                name_curso1 = str("curso asociado:"+fullname)

                #INSERCION GRUPO
                #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                url = BATUTA_URL
                #args = { 'wstoken' : BATUTA_TOKEN, #stage
                args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                    'wsfunction' : 'core_group_create_groups',
                    'groups[0][courseid]' : '',
                    'groups[0][name]' : '',
                    'groups[0][description]' : '',
                    'moodlewsrestformat' : 'json',
                    }
                args ['groups[0][courseid]'] = id_curso
                args ['groups[0][name]'] = group
                args ['groups[0][description]'] = name_curso1
                response = requests.post(url, data=args)
                response = response.json()
                print(response)
                core_group_create_groups('REGISTRO CURSO EN GRUPO EXITOSO ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]) + ' GRUPO: ' + group) if 'id' in response[0].keys() else core_group_create_groups('ERROR EN REGISTRO CURSO EN GRUPO ' + 'NID: ' + str(nid) + ' CURSO: ' + str(id_curso[0][0]) + ' GRUPO: ' + group)
                id_grupo = response[0]["id"]
                print("REGISTRO CURSO EN GRUPO EXITOSO")
                logging.info('REGISTRO GRUPO  %s EN CURSO c_id = %s EXITOSO',c_id)

                query = "SELECT id FROM mdl_user where phone2 = "+str(e_id)+" and deleted = 0"
                c.execute(query)
                anterior_id = c.fetchall()
                id_user = anterior_id

                #print("e_id", e_id,id_user)

                if id_user:
                    #--------------INSERCION MANUAL MIEMBROS GRUPOS------------------------------
                    url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        #'wstoken' : BATUTA_TOKEN,
                        'wsfunction' : 'enrol_manual_enrol_users',
                        'enrolments[0][roleid]' : 5,
                        'enrolments[0][userid]' : '',
                        'enrolments[0][courseid]' : '',
                        'enrolments[0][suspend]' : 0,
                        'moodlewsrestformat' : 'json',
                        }
                    args ['enrolments[0][userid]'] = id_user[0][0]
                    args ['enrolments[0][courseid]'] = id_curso

                    print("REGISTRO MIEMBRO ENROLLMENTS MANUAL EXITOSO")
                    logging.info('REGISTRO USUARIO NID = %s, AL CURSO %s EXITOSO',nid,c_id)

                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    enrol_manual_enrol_users('ERROR EN ENROLAR USUARIO '+ 'NID: ' + str(nid) + ' CURSO ' + str(id_curso[0][0])) if response != None else enrol_manual_enrol_users('USUARIO ENROLADO EXITOSAMENTE '+ 'NID ' + str(nid) + ' CURSO ' + str(id_curso[0][0]))

                    #------------------------INSERCION MIEMBROS DEL GRUPO----------------

                    query2 = "SELECT id FROM mdl_groups where  courseid = "+str(id_curso)+" and name = '"+group+"'"
                    c.execute(query2)
                    id_group = c.fetchall()
                    new_id_group = id_grupo

                    if new_id_group:

                        #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                        url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_group_add_group_members',
                            'members[0][groupid]' : '',
                            'members[0][userid]' : '',
                            'moodlewsrestformat' : 'json',
                            }
                        args ['members[0][groupid]'] = new_id_group
                        args ['members[0][userid]'] = id_user

                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_group_add_group_members('ERROR EN AGREGAR USUARIO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group)) if response != None else core_group_add_group_members('SE AGREGO USUARIO CON EXITO ' + 'NID: ' + str(nid) + ' AL GRUPO: ' + str(new_id_group))
                        print("REGISTRO MIEMBROS ENROLLMENTS DE GRUPO EXITOSO")
                        logging.info('REGISTRO USUARIO NID = %s, EN GRUPO %s EXITOSO',nid,group)

'''---------------------------------------------------CREACION CURSOS----------------------------------------------'''
#@csrf_exempt
def consulta_cursos():
    #periodo_sige()
    datos = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_courses",
        "at_id" : periodo_sige(),

        }
    )
    datos_cursos = datos["data"]["ar_courses"]
    try:
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        #con = mysql.connector.connect( host='localhost', user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()
    except:
        error = "ERROR CONEXION BASE DE DATOS STAGE"
        errores(error)

    # consultamos el campo u_id de la tabla mdl_user
    query = "SELECT idnumber FROM mdl_course"
    c.execute(query)
    myresult_mdl_courses = c.fetchall()

    #sacamos el número de datos que tiene sige
    cont_id_cursos = 0
    cont_id_cursos = len(datos_cursos)
    cont_id_tabla = 0

    lista_datos_cursos = []
    for item in datos_cursos.values():
        lista_datos_cursos.append(item)
        c_id = list(item.values())[0]
        name =  list(item.values())[27]
        #insert_groups_and_members(c_id,name)

        for i in myresult_mdl_courses:
            # condicion cuando el id course  esta en la tabla
            if c_id in i :
                #contador (cont_id_tabla) de los id en la tabla mdl_courses
                cont_id_tabla += 1

    nuevos_cursos= []
    # Validamos si el número de usuarios en sige es igual al número usuarios de la tabla mld_courses (BD batuta)
    if cont_id_cursos == cont_id_tabla:
        print("El ultimo registro de sige coincide con el ultimo registro curso de Moodle (iguales)")
        logging.info('El ultimo registro de sige coincide con el ultimo registro curso de Moodle (iguales)')

    # Validamos si el número de usuarios en sige es mayor al número de usuarios de la tabla mld_courses (BD batuta)

    elif cont_id_cursos > cont_id_tabla:
        print("El ultimo registro de la tabla ml_courses es diferente al ultimo registro cursos.")
        logging.info('El ultimo registro de la tabla ml_courses es diferente al ultimo registro cursos.')
        cont_i = 0
        for i in lista_datos_cursos:
          cont_i += 1

          if cont_i > cont_id_tabla:
              nuevos_cursos .append(i)
        return nuevos_cursos

    #return HttpResponse(json.dumps(nuevos_cursos), content_type="application/json")

'''---------------------------------------------------CREACION CATEGORIAS----------------------------------------------'''

#@csrf_exempt
def get_categories():
    try:
        # variable global para lograr que la funcion se ejecute totalmente y solo se retorne el tiempo cuando termine
        global time_category
        time_category  = True
        try:
            con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            #con = mysql.connector.connect( host='localhost', user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            c = con.cursor()
        except:
            error = "ERROR CONEXION BASE DE DATOS STAGE"
            errores(error)

        # consultamos el campo idnumber de la tabla mdl_course_categories
        query1 = "SELECT idnumber FROM mdl_course_categories"
        c.execute(query1)
        category = c.fetchall()

        # captura el valor del ultimo usuario de la funcion consulta_matricula
        consulta_funcion = consulta_cursos()
        #respuesta = ["Registro categoria"]
        datos_ultimo_curso = []
        if consulta_funcion:
            for i_consulta in consulta_funcion:
                datos_ultimo_curso.clear()

                for i in i_consulta.values():
                  datos_ultimo_curso.append(i)
                lv_id = datos_ultimo_curso[26]
                level_name = datos_ultimo_curso[32]

                cont_category = 0

                for i in category:
                    if lv_id in i :
                      cont_category += 1

                if cont_category >=1:
                    print("CATEGORIA REGISTRADA")
                    logging.info('CATEGORIA %s  REGISTRADA',level_name)

                elif cont_category == 0:

                    # INSERTA CATEGORY  metodo POST
                    #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                    url = BATUTA_URL
                    #args = { 'wstoken' : BATUTA_TOKEN, #stage
                    args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                        'wsfunction' : 'core_course_create_categories',
                        'categories[0][name]' : '',
                        'categories[0][idnumber]' : '',
                        'moodlewsrestformat' : 'json',
                        }

                    args ['categories[0][name]'] = level_name
                    args ['categories[0][idnumber]'] = lv_id

                    response = requests.post(url, data=args)
                    response = response.json()
                    print(response)
                    core_course_create_categories('REGISTRO DE CATEGORIA EXITOSO ' + str(lv_id)) if 'id' in response[0].keys() else core_course_create_categories('ERROR AL REGISTRAR CATEGORIA ' + str(lv_id))
        c.close()
    except:
        #print("ERROR GET_CATEGORIA CURSOS")
        error = "ERROR_GET_CATEGORIA"
        errores(error)
    #         respuesta = ["categoria"]
    time_category = False
    # return HttpResponse(json.dumps(respuesta), content_type="application/json")


#@csrf_exempt
def get_courses():
    print("Entra curso 2")
    try:
        # variable global para lograr que la funcion se ejecute totalmente y solo se retorne el tiempo cuando termine
        global time_courses
        time_courses = True
        try:
            con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            #con = mysql.connector.connect( host='localhost', user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
            c = con.cursor()
        except:
            error = "ERROR CONEXION BASE DE DATOS STAGE"
            errores(error)

        # consultamos el campo id de la tabla mdl_course
        query = "SELECT idnumber FROM mdl_course"
        c.execute(query)
        course = c.fetchall()

        query1 = "SELECT idnumber FROM mdl_course_categories"
        c.execute(query1)
        category = c.fetchall()

        # captura el valor del ultimo usuario de la funcion consulta_matricula
        consulta_funcion = consulta_cursos()
        datos_ultimo_curso = []

        if consulta_funcion:
            for i_consulta in consulta_funcion:
                datos_ultimo_curso.clear()

                for i in i_consulta.values():
                  datos_ultimo_curso.append(i)

                c_id = datos_ultimo_curso[0]
                lv_id = datos_ultimo_curso[26]
                name =  datos_ultimo_curso[27]
                fullname = datos_ultimo_curso[33]
                cont_category = 0

                for i in category:
                    if lv_id in i :
                      cont_category += 1

                if cont_category >=1:

                    datos_categoria = "SELECT id FROM mdl_course_categories WHERE idnumber = '"+str(lv_id)+"' "
                    c.execute(datos_categoria)
                    categoria = c.fetchall()
                    category_bd = categoria[0][0]

                    cont_id_bd_mdl_course = 0
                    for i in course:
                        # Validamos si id esta en la tabla mdl_course
                        if c_id in i :
                          cont_id_bd_mdl_course += 1

                    # if cont_id_bd_mdl_course >=1:
                    #     #print("---------CURSO REGISTRADO-----------")

                    if cont_id_bd_mdl_course == 0:

                        #url = 'http://stage-moodle.formacionvirtualbatuta.org/webservice/rest/server.php?'
                        url = BATUTA_URL
                        #args = { 'wstoken' : BATUTA_TOKEN, #stage
                        args = {'wstoken': BATUTA_TOKEN,  # moodleBatutaFinal
                            'wsfunction' : 'core_course_create_courses',
                            'courses[0][fullname]' : '',
                            'courses[0][shortname]' : '',
                            'courses[0][categoryid]' : '',
                            'courses[0][idnumber]' : '',
                            'moodlewsrestformat' : 'json',
                            }

                        args ['courses[0][fullname]'] = traducir(fullname)
                        args ['courses[0][shortname]'] = traducir(fullname)+"_"+str(periodo_sige())
                        args ['courses[0][categoryid]'] = category_bd
                        args ['courses[0][idnumber]'] = c_id
                        response = requests.post(url, data=args)
                        response = response.json()
                        print(response)
                        core_course_create_courses('CURSO REGISTRADO CON EXITO ' + 'C_ID: ' + str(c_id) + ' CATEGORIA: ' + str(category_bd)) if 'id' in response[0].keys() else core_course_create_courses('ERROR AL REGISTRAR CURSO ' + 'C_ID: ' + str(c_id) + ' CATEGORIA: ' + str(category_bd))
                        print("CURSO REGISTRADO EXITOSAMENTE")
                        logging.info('CURSO %s REGISTRADO EXITOSAMENTE',c_id)
                        time.sleep(0.2)


                # else:
                #     get_categories()
            # respuesta = ["Registro matricula"]
        c.close()
    except:
        #print("ERROR GET_CURSOS")
        error = "ERROR_GET_CURSOS"
        errores(error)
    time_courses = False
    #return HttpResponse(json.dumps(respuesta), content_type="application/json")


'''--------------------------------------------------ERRORES---------------------------------------'''

def errores(error):
    # borrar_txt('apps/Batuta/logs/create_categories_logs.txt')
    ahora = datetime.now() 
    fecha = str(ahora)
    # f = open('/data/www/webservice/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt','a')
    f = open('/var/www/batutaws_moodle/WebService/apps/Batuta/logs/errores.txt','a')
    f.write('\n' + fecha +" --> " + error)
    f.close()

def core_user_create_users(status):
    # borrar_txt('apps/Batuta/logs/create_categories_logs.txt')
    ahora = datetime.now()
    fecha = str(ahora)
    # f = open('/data/www/webservice/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt','a')
    f = open('/var/www/batutaws_moodle/WebService/apps/Batuta/logs/users_logs.txt','a')
    f.write('\n' + fecha +" --> " + status)
    f.close()

def enrol_manual_enrol_users(status):
    # borrar_txt('apps/Batuta/logs/create_categories_logs.txt')
    ahora = datetime.now()
    fecha = str(ahora)
    # f = open('/data/www/webservice/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt','a')
    f = open('/var/www/batutaws_moodle/WebService/apps/Batuta/logs/enrol_users_logs.txt','a')
    f.write('\n' + fecha +" --> " + status)
    f.close()

def core_course_create_courses(status):
    # borrar_txt('apps/Batuta/logs/create_categories_logs.txt')
    ahora = datetime.now()
    fecha = str(ahora)
    # f = open('/data/www/webservice/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt','a')
    f = open('/var/www/batutaws_moodle/WebService/apps/Batuta/logs/cursos_logs.txt','a')
    f.write('\n' + fecha +" --> " + status)
    f.close()

def core_group_add_group_members(status):
    # borrar_txt('apps/Batuta/logs/create_categories_logs.txt')
    ahora = datetime.now()
    fecha = str(ahora)
    # f = open('/data/www/webservice/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt','a')
    f = open('/var/www/batutaws_moodle/WebService/apps/Batuta/logs/add_group_members_logs.txt','a')
    f.write('\n' + fecha +" --> " + status)
    f.close()

def core_group_create_groups(status):
    # borrar_txt('apps/Batuta/logs/create_categories_logs.txt')
    ahora = datetime.now()
    fecha = str(ahora)
    # f = open('/data/www/webservice/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt','a')
    f = open('/var/www/batutaws_moodle/WebService/apps/Batuta/logs/create_groups_logs.txt','a')
    f.write('\n' + fecha +" --> " + status)
    f.close()

def core_course_create_categories(status):
    # borrar_txt('apps/Batuta/logs/create_categories_logs.txt')
    ahora = datetime.now()
    fecha = str(ahora)
    # f = open('/data/www/webservice/var/www/ws-bat//var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt','a')
    f = open('/var/www/batutaws_moodle/WebService/apps/Batuta/logs/create_categories_logs.txt','a')
    f.write('\n' + fecha +" --> " + status)
    f.close()

'''--------------------------------------------------COMPARACION CURSOS---------------------------------------'''

def comparacion_cursos():
    global  time_courses
    time_courses = True

    try:
        print("el entra aqui 0")
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()

    except sqlite3.Error as error:
        print('Error en base de datos sqlite')
        error = "ERROR CONEXION BASE DE DATOS DE COMPARACION"
        errores(error)

    datosCursos = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_courses",
        "at_id" : periodo_sige(),
        }
    )
    datos_cursos = datosCursos["data"]["ar_courses"]
    print("aqui van los cursos")
    print(datos_cursos)
    cont_cursos = len(datos_cursos)
    print("cont",cont_cursos)

    datos_cursos = "SELECT cont_users FROM bat_sqlite_cursos"
    c.execute(datos_cursos)
    bd_cursos = c.fetchall()
    bd_cursos = bd_cursos[0]

    print("cont",bd_cursos[0])

    if cont_cursos > bd_cursos[0]:

        sql = "UPDATE bat_sqlite_cursos SET cont_users = '"+str(cont_cursos)+"' WHERE id = 1 "
        c.execute(sql)
        con.commit()

        logging.info('NUEVOS CURSOS O CATEGORIAS')
        print("envia categorias")
        get_categories()
        print("envia")
        get_courses()
    else:
        logging.info('SIN CAMBIO EN EL NUMERO DE CURSOS %s|',cont_cursos)
        var_datos = "El ultimo registro teachers de sige coincide con el ultimo registro de Moodle (iguales)"

    time_courses = False

'''--------------------------------------------------COMPARACION TEACHERS--------------------------------------'''

def comparacion_teachers():
    global  time_teachers
    time_teachers = True

    try:
        print("el entra aqui 2")
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()

    except sqlite3.Error as error:
        print('Error en base de datos sqlite')
        error = "ERROR CONEXION BASE DE DATOS DE COMPARACION"
        errores(error)

    at_id = periodo_sige()
    datosTeachers = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_teachers",
        "security_branch_exclusion": 0,
        "force_search_current_branches": 0,
        "status": 1
        }
    )
    datosTeacher =  datosTeachers["data"]["ar_teachers"]
    cont_teachers = len(datosTeacher)

    datos_teachers = "SELECT cont_users FROM bat_sqlite_teachers"
    c.execute(datos_teachers)
    bd_teachers = c.fetchall()
    bd_teacher = int(bd_teachers[0][0])

    print("Comparacion de bd teacher yteachers ",bd_teacher,"  ",cont_teachers)
    #print(bd_teacher,bd_teachers)
    if cont_teachers > bd_teacher:
        sql = "UPDATE bat_sqlite_teachers SET cont_users = '"+str(cont_teachers)+"' WHERE id = 1 "
        c.execute(sql)
        con.commit()
        con.close()
        logging.info('NUEVOS TEACHERS')
        get_teachers()

    logging.info('SIN CAMBIO EN EL NUMERO DE TEACHERS |')

    time_teachers = False

'''--------------------------------------------------COMPARACION ACTUALIZACION TEACHERS---------------------------------------'''

def comparacion_update_teachers():

    global  time_update_teachers
    time_update_teachers = True

    try:
        # conexion= base_datos_sqlite()
        # cursor = conexion.cursor()
        print("el entra aqui 3")
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()

    except sqlite3.Error as error:
        print('Error en base de datos sqlite')
        error = "ERROR CONEXION BASE DE DE COMPARACION"
        errores(error)

    ahora = datetime.now()
    fecha = str(ahora)

    lista_new_enrollments = []
    datos = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_modified_users",
        "role_id": "140",
        "min_date": str(time_now()),
        # "min_date": "2022-07-01",
        "force_search_current_branches" : 0
        }
    )

    if datos["data"]['ar_users']:
        print("Entramos por que hay data")
        print(datos)
        datos_matriculados = datos["data"]["ar_users"]
        print("************************")
        print(datos_matriculados)

        datos_ultimo_usuario = []
        
        for i_consulta in datos_matriculados.values():
            print("Vemos que es i_consulta en techers ",i_consulta)
            datos_ultimo_usuario.clear()
            for i in i_consulta.values():
              datos_ultimo_usuario.append(i)

            u_id = datos_ultimo_usuario[0]
            nid = datos_ultimo_usuario[1]
            first_name = datos_ultimo_usuario[4]
            last_name = datos_ultimo_usuario[5]
            bid = datos_ultimo_usuario[15]

            sql = "SELECT u_id, nid, first_name, last_name, bid FROM bat_teachers WHERE u_id = "+str(u_id)+""
            c.execute(sql)
            result = c.fetchall()


            if result:  # Validamos si idnumber esta en la tabla mdl_user
                print("dato db",result)
                datos_uid = (result[0][0])
                datos_nid = (result[0][1])
                datos_first_name = (result[0][2])
                datos_last_name = (result[0][3])
                datos_bid = (result[0][4])

                if datos_nid != nid or datos_first_name != first_name or datos_last_name != last_name or datos_bid != bid:
                    print("cambiodatoss")
                    lista_new_enrollments.append(i_consulta)
                    sql = "UPDATE bat_teachers SET nid = '"+str(nid)+"', first_name = '"+str(first_name)+"', last_name = '"+str(last_name)+"', bid = '"+str(bid)+"' WHERE u_id = '"+str(u_id)+"' "

                at_id = periodo_sige()
                datos = getData(
                {
                "school_api_key": SCHOOL_API_KEY,
                "user_api_key": USER_API_KEY,
                "action": "get_teaching_load",
                "u_id": u_id,
                "at_id": at_id
                }
                )

                if datos["data"]:
                    datosTeacherxcourse = datos["data"]["teaching_load"]
                    #print("datos sige",datos["data"]["teaching_load"])

                    datos_groupss = []
                    for i_curso in datosTeacherxcourse.values():
                        datos_groupss.clear()
                        for data in i_curso.values():
                            datos_groupss.append(data)

                        c_id = datos_groupss[4]
                        group = datos_groupss[5]
                        print("c y g",c_id,group)

                        sql1 = "SELECT id FROM bat_update_course_group WHERE u_id = "+str(u_id)+" and course = "+str(c_id)+" and groups_bat = '"+str(group)+"' "
                        c.execute(sql1)
                        cursos_grupos= c.fetchall()

                        if not cursos_grupos:
                            lista_new_enrollments.append(i_consulta)
                            sql = "INSERT INTO bat_update_course_group (u_id , course, groups_bat ) VALUES (%s,%s,%s)"
                            val = (u_id, c_id, group)
                            c.execute(sql, val)
                            con.commit()

                            if datos_nid != nid or datos_first_name != first_name or datos_last_name != last_name or datos_bid != bid:
                                lista_new_enrollments.append(i_consulta)
                                sql = "UPDATE bat_teachers SET nid = '"+str(nid)+"', first_name = '"+str(first_name)+"', last_name = '"+str(last_name)+"', bid = '"+str(bid)+"' WHERE u_id = '"+str(u_id)+"' "

                        else:
                            if datos_nid != nid or datos_first_name != first_name or datos_last_name != last_name or datos_bid != bid:
                                lista_new_enrollments.append(i_consulta)
                                sql = "UPDATE bat_teachers SET nid = '"+str(nid)+"', first_name = '"+str(first_name)+"', last_name = '"+str(last_name)+"', bid = '"+str(bid)+"' WHERE u_id = '"+str(u_id)+"' "

                            else:
                                logging.info('SIN ACTUALIZACIONES EN ENROLLMENTS |')
                                print("SIN ACTUALIZACIONES EN ENROLLMENTS ")

            else:

                lista_new_enrollments.append(i_consulta)
                print("#########################################")
                print("#########################################")
                print(lista_new_enrollments)
                print("#########################################")
                print("#########################################")
                sql = "INSERT INTO bat_teachers (u_id , nid, first_name, last_name, bid) VALUES (%s,%s,%s,%s,%s)"
                val = (u_id, nid, first_name, last_name, bid)
                c.execute(sql, val)
                con.commit()

                at_id = periodo_sige()
                datos = getData(
                {
                "school_api_key": SCHOOL_API_KEY,
                "user_api_key": USER_API_KEY,
                "action": "get_teaching_load",
                "u_id": u_id,
                "at_id": at_id
                }
                )

                if datos["data"]:
                    datosTeacherxcourse = datos["data"]["teaching_load"]
                    datos_groupss = []
                    for i in datosTeacherxcourse.values():
                        datos_groupss.clear()
                        for data in i.values():
                            datos_groupss.append(data)

                        c_id = datos_groupss[4]
                        group = datos_groupss[5]

                        sql = "INSERT INTO bat_update_course_group (u_id, course, groups_bat ) VALUES (%s,%s,%s)"
                        val = (u_id, c_id, group)
                        c.execute(sql, val)
                        con.commit()
        else:
            logging.info('SIN ACTUALIZACIONES EN TEACHERS |')
            print("SIN ACTUALIZACIONES EN TEACHERS ")

    else:
        logging.info('SIN ACTUALIZACIONES EN TEACHERS |')

    c.close()
    dato_uid = []
    print("Esperamos 2 segs")
    time.sleep(2)
    print(lista_new_enrollments)

    for i_consulta in lista_new_enrollments:
        dato_uid.clear()

        for i in i_consulta.values():
            dato_uid.append(i)

        uid = dato_uid[0]
        nid = dato_uid[1]
        print(uid)

        teachers_x_courses(uid,nid)

    time_update_teachers = False

'''--------------------------------------------------COMPARACION NUEVOS ENROLLMENTS---------------------------------------'''

def comparacion_nuevos_enrollments():

    global time_new_enrollments
    time_new_enrollments = True

    try:
        print("el entra aqui 1")
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        #print("pero aqui no")
        c = con.cursor()

    except sqlite3.Error as error:
        print('Error en base de datos sqlite')
        error = "ERROR CONEXION BASE DE COMPARACION"
        errores(error)

    sql = "SELECT MAX(phone2) FROM mdl_user where deleted = 0"
    # sql = "SELECT MIN(phone2) FROM mdl_user WHERE phone2 != '' and  phone2 != 12345 "
    c.execute(sql)
    result = c.fetchall()
    max = int(result[0][0])
    max_db = max+1
    print("Aqui traemos Max y max_db")
    print('Maximo e_id SIGE '+str(max))
    print('Maximo e_id BD '+str(max_db))

    #Extraemos de entre todos los e_id el minimo para hacer consulta de 10 en 10 registros
    max_db = int(min_e_id_sige(max_db))
    print("nuevo max_db" , max_db)
    #time.sleep()

    # sql = "SELECT e_id FROM bat_max_eid"
    # c.execute(sql)
    # result = c.fetchall()
    # max_eid = result[0][0]
    # max_eid = int(max_eid)

    lista_new_enrollments = []
    print("el periodo de sige es ",periodo_sige())
    datos = getData(
            {
            "school_api_key": SCHOOL_API_KEY,
            "user_api_key": USER_API_KEY,
            "action": "get_enrollments",
            "status": 0,
            "at_id":periodo_sige(),
            "min_e_id": max_db,
            "max_e_id": max_db+9,
            "force_search_current_branches" : 0
            }
        )
    print("Los ",len(datos["data"]["ar_enrollments"])," datos")
    print(datos)
    #print("traemos datos sige",len(datos["data"]))
    if datos["data"]:
        datos_matriculados = datos["data"]["ar_enrollments"]
        cont_matriculados = len(datos_matriculados)
        print("Tambien miramos que hay en cont_matr")
        print(cont_matriculados)
        logging.info('NUEVOS ENROLLMENTS: %s',cont_matriculados)
        sql = "UPDATE bat_max_eid SET e_id = '"+str(max)+"' WHERE id = 1 "
        c.execute(sql)
        con.commit()

        for i in datos_matriculados.values():
            lista_new_enrollments.append(i)
        print("Esto es lo primero que me traigo de new_enrollments")
        print(lista_new_enrollments[-1])
    else:
        print("sale por el numero de enrollemts no cambia")
        logging.info('SIN CAMBIO EN EL NUMERO DE NUEVOS ENROLMENTS, MAXIMO E_ID %s',max_db )

    c.close()
    #time.sleep(5)
    #print(lista_new_enrollments)

    if lista_new_enrollments:
        insert_new_enrollments(lista_new_enrollments)

    time_new_enrollments = False

'''--------------------------------------------------COMPARACION ACTUALIZACION ENROLLMENTS---------------------------------------'''

def comparacion_update_enrollments():

    global  time_update_enrollments
    time_update_enrollments = True
    print("entra")

    try:
        print("el entra aqui")
        con = mysql.connector.connect( host=BATUTA_HOST, user= BATUTA_USER, passwd=BATUTA_PASSWORD, db=BATUTA_DB, port=BATUTA_PORT)
        c = con.cursor()

    except sqlite3.Error as error:
        print('Error en base de datos sqlite')
        error = "ERROR CONEXION BASE DE COMPARACION"
        errores(error)


    lista_new_enrollments = []
    fecha = datetime.today().strftime('%Y-%m-%d')
    # print(fecha)
    # print("fechaaa", fecha)

    datos = getData(
        {
        "school_api_key": SCHOOL_API_KEY,
        "user_api_key": USER_API_KEY,
        "action": "get_modified_enrollments",
        "min_date": str(time_now()),
        # "min_date": "2022-07-01",
        "force_search_current_branches" : 0
        }
    )
    print("datosss",datos["data"]["ar_enrollments"])
    if datos["data"]["ar_enrollments"]:
        datos_matriculados = datos["data"]["ar_enrollments"]
        datos_ultimo_usuario = []
        for i_consulta in datos_matriculados.values():
            datos_ultimo_usuario.clear()
            for i in i_consulta.values():
              datos_ultimo_usuario.append(i)

            e_id = i_consulta["e_id"]        
            u_id = i_consulta["u_id"]
            first_name = i_consulta["first_name"]
            last_name = i_consulta["last_name"]
            login = i_consulta["login"]
            nid = i_consulta["nid"]
            nid= nid.lower()
            bid = i_consulta["b_id"]

            sql = "SELECT u_id, nid, e_id, first_name, last_name, bid FROM bat_enrollments WHERE u_id = "+str(u_id)+""
            c.execute(sql)
            result = c.fetchall()


            if result:  # Validamos si idnumber esta en la tabla mdl_user

                datos_uid = (result[0][0])
                datos_nid = (result[0][1])
                datos_eid = (result[0][2])
                datos_first_name = (result[0][3])
                datos_last_name = (result[0][4])
                datos_bid = (result[0][5])

                if datos_eid != e_id:
                    print("cambio e_id")
                    if datos_nid != nid or datos_first_name != first_name or datos_last_name != last_name or datos_bid != bid:
                        print("cambiodatoss")
                        lista_new_enrollments.append(i_consulta)
                        sql = "UPDATE bat_enrollments SET nid = '"+str(nid)+"', e_id = '"+str(e_id)+"', first_name = '"+str(first_name)+"', last_name = '"+str(last_name)+"', bid = '"+str(bid)+"' WHERE u_id = '"+str(u_id)+"' "
                    else:
                        lista_new_enrollments.append(i_consulta)
                        sql = "UPDATE bat_enrollments SET e_id = '"+str(e_id)+"' WHERE u_id = '"+str(u_id)+"' "

                else:
                    datos_group_sige = getData(
                                {
                                "school_api_key": SCHOOL_API_KEY,
                                "user_api_key": USER_API_KEY,
                                "action": "get_groups_x_course_x_enrollment",
                                "e_id" : e_id,
                                "ask_for_e_id" : 0,
                                "index" : "c_id"
                                }
                            )

                    if datos_group_sige["data"]:
                        datos_group = datos_group_sige["data"]["ar_groups_x_course_x_enrollment"]
                        datos_groupss = []
                        for i_curso in datos_group.values():
                            datos_groupss.clear()
                            for data in i_curso.values():
                                datos_groupss.append(data)

                            c_id = datos_groupss[1]
                            group = datos_groupss[2]
                            print("c y g",c_id,group)

                            sql1 = "SELECT id FROM bat_update_course_group WHERE u_id = "+str(u_id)+" and course = "+str(c_id)+" and groups_bat = '"+str(group)+"' "
                            c.execute(sql1)
                            cursos_grupos= c.fetchall()

                            if not cursos_grupos:
                                lista_new_enrollments.append(i_consulta)
                                sql = "INSERT INTO bat_update_course_group (u_id , course, groups_bat ) VALUES (%s,%s,%s)"
                                val = (u_id, c_id, group)
                                c.execute(sql, val)
                                con.commit()

                                if datos_nid != nid or datos_first_name != first_name or datos_last_name != last_name or datos_bid != bid:
                                    lista_new_enrollments.append(i_consulta)
                                    sql = "UPDATE bat_enrollments SET nid = '"+str(nid)+"', e_id = '"+str(e_id)+"', first_name = '"+str(first_name)+"', last_name = '"+str(last_name)+"', bid = '"+str(bid)+"' WHERE u_id = '"+str(u_id)+"' "

                            else:
                                if datos_nid != nid or datos_first_name != first_name or datos_last_name != last_name or datos_bid != bid:
                                    lista_new_enrollments.append(i_consulta)
                                    sql = "UPDATE bat_enrollments SET nid = '"+str(nid)+"', e_id = '"+str(e_id)+"', first_name = '"+str(first_name)+"', last_name = '"+str(last_name)+"', bid = '"+str(bid)+"' WHERE u_id = '"+str(u_id)+"' "

                                else:
                                    logging.info('SIN ACTUALIZACIONES EN ENROLLMENTS |')
                                    print("SIN ACTUALIZACIONES EN ENROLLMENTS ")

            else:

                lista_new_enrollments.append(i_consulta)
                sql = "INSERT INTO bat_enrollments (u_id , nid, e_id, first_name, last_name, bid) VALUES (%s,%s,%s,%s,%s,%s)"
                val = (u_id, nid, e_id, first_name, last_name, bid)
                c.execute(sql, val)
                con.commit()

                datos_group_sige = getData(
                                {
                                "school_api_key": SCHOOL_API_KEY,
                                "user_api_key": USER_API_KEY,
                                "action": "get_groups_x_course_x_enrollment",
                                "e_id" : e_id,
                                "ask_for_e_id" : 0,
                                "index" : "c_id"
                                }
                            )

                if datos_group_sige["data"]:
                    datos_group = datos_group_sige["data"]["ar_groups_x_course_x_enrollment"]
                    datos_groupss = []
                    for i in datos_group.values():
                        datos_groupss.clear()
                        for data in i.values():
                            datos_groupss.append(data)

                        c_id = datos_groupss[1]
                        group = datos_groupss[2]

                        sql = "INSERT INTO bat_update_course_group (u_id, course, groups_bat ) VALUES (%s,%s,%s)"
                        val = (u_id, c_id, group)
                        c.execute(sql, val)
                        con.commit()
    else:
        logging.info('SIN ACTUALIZACIONES EN ENROLLMENTS |')
        print("SIN ACTUALIZACIONES EN ENROLLMENTS ")

    c.close()
    print("Vamos por aqui")
    print(lista_new_enrollments)
    insert_update_enrollments(lista_new_enrollments)

    time_update_enrollments = False

'''--------------------------------------------------POOL---------------------------------------'''

def inicial_pool():

    ###logging.basicConfig(level=###logging.DEBUG, format='%(threadName)s: %(message)s')
    executor = ThreadPoolExecutor(max_workers=1)
    logging.info('variables globales de cursos, categoria y nuevas sedes: %s',time_courses)
    print("Entrta al pool 1")
    if time_courses == False:
        future = executor.submit(comparacion_cursos)
        if as_completed(future):
            print(future.result())
    executor.shutdown()

def inicial_pool_2():

    ###logging.basicConfig(level=###logging.DEBUG, format='%(threadName)s: %(message)s')
    executor = ThreadPoolExecutor(max_workers=4)
    logging.info('variables globales de teacher,actualizacion teachers y  enrollments: %s,%s',time_teachers, time_update_teachers)


    if funcion_response() == "Estable":
        if time_new_enrollments == False:
            #future = executor.submit(comparacion_nuevos_enrollments)
            """if as_completed(future):
                print("el futuro es completado")
                print(future.result())
                print("futuro depues del result")
            else: 
                print("NOT COMPLETED")"""
            #Comparacion nuevos enrollments sin hilos por motivo de malfuncionamiento (Insercion de usuarios incompleta)
            print("Enta sin hilo")
            comparacion_nuevos_enrollments()
            
        else:
            print("time_new_enrollmets es TRue")
        
    else:
        time_new_enrollments1 = funcion_response()

    if funcion_response() == "Estable":
        if time_update_enrollments == False:
            future = executor.submit(comparacion_update_enrollments)
            print(future.result())
    else:
        time_update_enrollments1 = funcion_response()


    if funcion_response() == "Estable":
        if time_teachers == False:
            future = executor.submit(comparacion_teachers)
            print(future.result())
    else:
        time_teachers1 = funcion_response()

    if funcion_response() == "Estable":
        if time_update_teachers == False:
            future = executor.submit(comparacion_update_teachers)
            print(future.result())
        else:
            time_update_teachers1 = funcion_response()
    executor.shutdown()

    # if time_new_enrollments1 == "Falla_servidor" or time_update_enrollments1 == "Falla_servidor" or time_teachers1 == "Falla_servidor" or time_update_teachers1 == "Falla_servidor":

    #     webbrowser.open_new('http://ws-bta.egteamdev.tk')


'''_______________________________________________function_logging____________________________________'''

def funcion_response():
    r = requests.get('http://ws-bta2.egteamdev.tk', timeout=5)
    if r.status_code == 200:
        status = "Estable"
        print('Estable',status)
        return status
    else:
        status = "Falla_servidor"
        print('Falla',status)
        return status


'''_____________________________________________________QUERY_TIME____________________________________'''

def query_time():
    def demora(tiempo,mensaje):
        global time_query
        time_query = True
        cada_cuanto =  sched.scheduler(time.time,time.sleep)
        cada_cuanto.enterabs(tiempo,1,print,argument=(mensaje,))
        print("Se mostrara un mensaje cada: 5 min ")
        print(datetime.now())
        logging.info('Se mostrara // un mensaje // cada: 5 min')
        cada_cuanto.run()


        hora = datetime.now().strftime('%H')

        inicial_pool()
        logging.info('INICIA PRIMER POOL')
        # time.sleep(1)
        logging.info('INICIA SEGUNDO POOL')
        inicial_pool_2()


        with open('/var/www/batutaws_moodle/WebService/apps/Batuta/testing.log', 'w'):
           pass

        open("/var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt", "w").close()

        time_query = False

    while True:
        demora(time.time()+30,"ejecutando una funcion")

def inicia_query_time():

    if time_query == False:
       query_time()

    else:
        #global Variable
        var = "Ejecutando funcion ( ‾ʖ̫‾)"
        return var

#--------------------------TESTING DATOS------------------------
#@csrf_exempt
def testing_datos(request):
    with open("/var/www/batutaws_moodle/WebService/apps/Batuta/testing.log",'r') as f:
        lines=[f.read()]
        line= f.read().splitlines()
        respuesta = line
    print(line)
    return render(request, '../templates/Batuta/testing_datos.html', {'respuesta': respuesta})


#--------------------------TESTING ERRORES------------------------
"""@csrf_exempt
def testing_errores(request):

    with open("/var/www/batutaws_moodle/WebService/apps/Batuta/errores.txt",'r') as f:

        respuesta=[f.read()]
        print(type(respuesta))
        print(respuesta)

    return render(request, '../templates/Batuta/testing_errores.html', {'respuesta': respuesta})
"""
@csrf_exempt
def lista_errores(request):
    listado, registro, enrol_users, registro_cursos = [],[],[],[]
    add_group_members_logs, create_groups_logs, create_categories_logs = [],[],[]

    logs_content = [
        (listado, "/var/www/batutaws_moodle/WebService/apps/Batuta/logs/errores.txt"),
        (registro, "/var/www/batutaws_moodle/WebService/apps/Batuta/logs/users_logs.txt"),
        (enrol_users, "/var/www/batutaws_moodle/WebService/apps/Batuta/logs/enrol_users_logs.txt"),
        (registro_cursos, "/var/www/batutaws_moodle/WebService/apps/Batuta/logs/cursos_logs.txt"),
        (add_group_members_logs, "/var/www/batutaws_moodle/WebService/apps/Batuta/logs/add_group_members_logs.txt"),
        (create_groups_logs, "/var/www/batutaws_moodle/WebService/apps/Batuta/logs/create_groups_logs.txt"),
        (create_categories_logs, "/var/www/batutaws_moodle/WebService/apps/Batuta/logs/create_categories_logs.txt")
    ]
    # Borrar logs
    if request.method == 'POST':
        name_file = str("apps/Batuta/logs/"+request.POST.get('tag')+".txt")
        borrar_txt(name_file)

    for log in logs_content:
        with open(log[1],'r') as file:
            for line in file:
                log[0].append(line)
    
    return render(request, '../templates/Batuta/testing_errores.html', {
        'listado': listado,
        'registro':  registro,
        'enrol_users': enrol_users,
        'regsitro_cursos': registro_cursos,
        'add_group_members_logs': add_group_members_logs,
        'create_groups_logs': create_groups_logs,
        'create_categories_logs': create_categories_logs})

#------------------------------------------------PAGINA INICIAL ------------------------------------
@csrf_exempt
def page_inicial(request):
    t = threading.Thread(target = inicia_query_time)
    t.start()
    var = inicia_query_time()
    if var == "Ejecutando funcion ¯\_( ͡❛ ͜ʖ ͡❛)_/¯ ":
       respuesta=[var]
    else:

       respuesta=["ok"]
    #respuesta=["ok"]

    return render(request, '../templates/Batuta/home.html', {'respuesta': respuesta})
"""
    función que retorna palabra sin tildes.

"""
def traducir(word):
    a,b = 'áéíóúüñÁÉÍÓÚÜÑ','aeiouunAEIOUUN'
    trans = str.maketrans(a,b)
    return word.translate(trans)

def time_now():
    format_data = '%Y-%m-%d'
    return str(datetime.now().strftime(format_data))

"""
Función que limpia los archivos txt de los logs del archivo testing_errores.html
"""
def borrar_txt(delete_file):
  archivo = open(delete_file,'w')
  archivo.write("")
  archivo.close()

def min_e_id_sige(max_db):
    a = getData(
            {
            "school_api_key": SCHOOL_API_KEY,
            "user_api_key": USER_API_KEY,
            "action": "get_enrollments",
            "status": 0,
            "at_id":periodo_sige(),
            "min_e_id": max_db,
            "force_search_current_branches" : 0
            }
        )
    if a['data']:

        b= a['data']['ar_enrollments']
        lista=[]
        for i in b:
            lista.append(i)
        lista= sorted(lista)
        return lista[0]
    else:
        return max_db
