from webbrowser import get
from django.urls import path
from rest_framework.routers import DefaultRouter

from rest_framework_swagger.views import get_swagger_view
from rest_framework.documentation import include_docs_urls

schema_view = get_swagger_view(title='Documentación WebService Batuta')


from apps.Batuta.views import *

#creamos la variable router
router = DefaultRouter()
#luego la registramos
#router.register('v1/users', MdlUserViewSet, basename='users')

urlpatterns = [
    path('',page_inicial,name='/templates/Batuta/home.html'),
    path('testing-datos/',testing_datos,name='testing-datos'),
    path('testing-errores/',lista_errores,name='testing-errores'),
    path("swagger-docs/", schema_view),
    path('coreapi-docs/', include_docs_urls(title='Documentación COREAPI')), 
]

urlpatterns += router.urls