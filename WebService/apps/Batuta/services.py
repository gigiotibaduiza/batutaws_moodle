import requests

def generate_request(parametros):
    #headers = {'Content-Type': 'none'}
    #url='http://plataforma.devteameg.tk//plugin/chamilo_app/rest.php'
    url='https://batuta.ciudadeducativa.com/app/web/api/index.php'
    response = requests.post(url, data = parametros)
    if response.status_code == 200:
        #guardar los datos(opcional)
        return response.json()

def getData(parametros):
    response = generate_request(parametros)
    if response:
        return response

# ps = generate_request()
# print(ps)